import 'package:flutter/material.dart';
import 'package:marketplace_bsa/screens/screen_product_id.dart';
import 'package:marketplace_bsa/screens/screen_products.dart';
import 'package:marketplace_bsa/services/products_api_provider.dart';

import 'models/product.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final List<Product> list = [];

  @override
  void initState() {
    fetchProducts().then((value) {
      setState(() {
        list.addAll(value);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(),
        body: ScreenProducts(
          list: list,
        ),
      ),
    );
  }

  Future<List<Product>> fetchProducts() async {
    return await ProductsProvider().loadProducts();
  }
}
