import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:marketplace_bsa/models/id/product_id.dart';
import 'package:marketplace_bsa/services/product_id_api_provider.dart';
import 'package:marketplace_bsa/widget/widget_pic.dart';

class ScreenProductId extends StatelessWidget {
  final int id;
  ScreenProductId({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
          child: FutureBuilder<ProductId>(
        future: fetchProductId(id),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            log(snapshot.data!.toString());
            return Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.keyboard_arrow_left),
                    ),
                    WidgetPic(picture: /*snapshot.data */ null),
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.keyboard_arrow_right),
                    ),
                  ],
                ),
              ],
            ) /* Text(snapshot.data!.title)*/;
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return Center(
            child: const CircularProgressIndicator(),
          );
        },
      )),
    );
  }

  Future<ProductId> fetchProductId(id) async {
    return await ProductIdProvider().loadProduct(id);
  }
}
