import 'package:flutter/material.dart';
import 'package:marketplace_bsa/models/product.dart';
import 'package:marketplace_bsa/widget/widget_pic.dart';
import 'package:marketplace_bsa/screens/screen_product_id.dart';

class ScreenProducts extends StatelessWidget {
  late final List<Product> list;
  ScreenProducts({Key? key, required this.list}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        padding: EdgeInsets.all(2),
        itemCount: list.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ScreenProductId(id: list[index].id),
                ),
              );
            },
            child: Container(
              height: 150,
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(2),
                  ),
                  border: Border.all(
                    width: 1,
                    color: Colors.black45,
                  )),
              padding: EdgeInsets.all(5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: 140,
                    height: 140,
                    child: WidgetPic(
                      picture: list[index].picture,
                    ),
                  ),
                  Column(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          width: MediaQuery.of(context).size.width - 240,
                          child: Text(list[index].title),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Container(
                          width: MediaQuery.of(context).size.width - 240,
                          child: Text(
                            list[index].description,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Container(
                      width: 44,
                      child: Text(
                        '\$${list[index].price}',
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

