import 'package:freezed_annotation/freezed_annotation.dart';

part 'pictures.freezed.dart';
part 'pictures.g.dart';

@freezed
class Pictures with _$Pictures {
  const Pictures._();
  @JsonSerializable(explicitToJson: true)
  const factory Pictures({
    required int? id,
    required String? url,
  }) = _Pictures;

  factory Pictures.fromJson(Map<String, dynamic> json) =>
      _$PicturesFromJson(json);
}
