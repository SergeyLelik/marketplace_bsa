// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pictures.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Pictures _$$_PicturesFromJson(Map<String, dynamic> json) => _$_Pictures(
      id: json['id'] as int?,
      url: json['url'] as String?,
    );

Map<String, dynamic> _$$_PicturesToJson(_$_Pictures instance) =>
    <String, dynamic>{
      'id': instance.id,
      'url': instance.url,
    };
