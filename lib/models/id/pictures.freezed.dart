// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'pictures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Pictures _$PicturesFromJson(Map<String, dynamic> json) {
  return _Pictures.fromJson(json);
}

/// @nodoc
class _$PicturesTearOff {
  const _$PicturesTearOff();

  _Pictures call({required int? id, required String? url}) {
    return _Pictures(
      id: id,
      url: url,
    );
  }

  Pictures fromJson(Map<String, Object?> json) {
    return Pictures.fromJson(json);
  }
}

/// @nodoc
const $Pictures = _$PicturesTearOff();

/// @nodoc
mixin _$Pictures {
  int? get id => throw _privateConstructorUsedError;
  String? get url => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PicturesCopyWith<Pictures> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PicturesCopyWith<$Res> {
  factory $PicturesCopyWith(Pictures value, $Res Function(Pictures) then) =
      _$PicturesCopyWithImpl<$Res>;
  $Res call({int? id, String? url});
}

/// @nodoc
class _$PicturesCopyWithImpl<$Res> implements $PicturesCopyWith<$Res> {
  _$PicturesCopyWithImpl(this._value, this._then);

  final Pictures _value;
  // ignore: unused_field
  final $Res Function(Pictures) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? url = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$PicturesCopyWith<$Res> implements $PicturesCopyWith<$Res> {
  factory _$PicturesCopyWith(_Pictures value, $Res Function(_Pictures) then) =
      __$PicturesCopyWithImpl<$Res>;
  @override
  $Res call({int? id, String? url});
}

/// @nodoc
class __$PicturesCopyWithImpl<$Res> extends _$PicturesCopyWithImpl<$Res>
    implements _$PicturesCopyWith<$Res> {
  __$PicturesCopyWithImpl(_Pictures _value, $Res Function(_Pictures) _then)
      : super(_value, (v) => _then(v as _Pictures));

  @override
  _Pictures get _value => super._value as _Pictures;

  @override
  $Res call({
    Object? id = freezed,
    Object? url = freezed,
  }) {
    return _then(_Pictures(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_Pictures extends _Pictures {
  const _$_Pictures({required this.id, required this.url}) : super._();

  factory _$_Pictures.fromJson(Map<String, dynamic> json) =>
      _$$_PicturesFromJson(json);

  @override
  final int? id;
  @override
  final String? url;

  @override
  String toString() {
    return 'Pictures(id: $id, url: $url)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Pictures &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.url, url));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(url));

  @JsonKey(ignore: true)
  @override
  _$PicturesCopyWith<_Pictures> get copyWith =>
      __$PicturesCopyWithImpl<_Pictures>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PicturesToJson(this);
  }
}

abstract class _Pictures extends Pictures {
  const factory _Pictures({required int? id, required String? url}) =
      _$_Pictures;
  const _Pictures._() : super._();

  factory _Pictures.fromJson(Map<String, dynamic> json) = _$_Pictures.fromJson;

  @override
  int? get id;
  @override
  String? get url;
  @override
  @JsonKey(ignore: true)
  _$PicturesCopyWith<_Pictures> get copyWith =>
      throw _privateConstructorUsedError;
}
