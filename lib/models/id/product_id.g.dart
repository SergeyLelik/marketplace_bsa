// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_id.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ProductId _$$_ProductIdFromJson(Map<String, dynamic> json) => _$_ProductId(
      id: json['id'] as int,
      title: json['title'] as String,
      price: json['price'] as int,
      description: json['description'] as String,
      createdAt: json['createdAt'] as String,
      pictures: (json['pictures'] as List<dynamic>)
          .map((e) => Pictures.fromJson(e as Map<String, dynamic>))
          .toList(),
      seller: Seller.fromJson(json['seller'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_ProductIdToJson(_$_ProductId instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'price': instance.price,
      'description': instance.description,
      'createdAt': instance.createdAt,
      'pictures': instance.pictures.map((e) => e.toJson()).toList(),
      'seller': instance.seller.toJson(),
    };
