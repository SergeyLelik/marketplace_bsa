// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'product_id.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProductId _$ProductIdFromJson(Map<String, dynamic> json) {
  return _ProductId.fromJson(json);
}

/// @nodoc
class _$ProductIdTearOff {
  const _$ProductIdTearOff();

  _ProductId call(
      {required int id,
      required String title,
      required int price,
      required String description,
      required String createdAt,
      required List<Pictures> pictures,
      required Seller seller}) {
    return _ProductId(
      id: id,
      title: title,
      price: price,
      description: description,
      createdAt: createdAt,
      pictures: pictures,
      seller: seller,
    );
  }

  ProductId fromJson(Map<String, Object?> json) {
    return ProductId.fromJson(json);
  }
}

/// @nodoc
const $ProductId = _$ProductIdTearOff();

/// @nodoc
mixin _$ProductId {
  int get id => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  int get price => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  String get createdAt => throw _privateConstructorUsedError;
  List<Pictures> get pictures => throw _privateConstructorUsedError;
  Seller get seller => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductIdCopyWith<ProductId> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductIdCopyWith<$Res> {
  factory $ProductIdCopyWith(ProductId value, $Res Function(ProductId) then) =
      _$ProductIdCopyWithImpl<$Res>;
  $Res call(
      {int id,
      String title,
      int price,
      String description,
      String createdAt,
      List<Pictures> pictures,
      Seller seller});

  $SellerCopyWith<$Res> get seller;
}

/// @nodoc
class _$ProductIdCopyWithImpl<$Res> implements $ProductIdCopyWith<$Res> {
  _$ProductIdCopyWithImpl(this._value, this._then);

  final ProductId _value;
  // ignore: unused_field
  final $Res Function(ProductId) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? title = freezed,
    Object? price = freezed,
    Object? description = freezed,
    Object? createdAt = freezed,
    Object? pictures = freezed,
    Object? seller = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      createdAt: createdAt == freezed
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String,
      pictures: pictures == freezed
          ? _value.pictures
          : pictures // ignore: cast_nullable_to_non_nullable
              as List<Pictures>,
      seller: seller == freezed
          ? _value.seller
          : seller // ignore: cast_nullable_to_non_nullable
              as Seller,
    ));
  }

  @override
  $SellerCopyWith<$Res> get seller {
    return $SellerCopyWith<$Res>(_value.seller, (value) {
      return _then(_value.copyWith(seller: value));
    });
  }
}

/// @nodoc
abstract class _$ProductIdCopyWith<$Res> implements $ProductIdCopyWith<$Res> {
  factory _$ProductIdCopyWith(
          _ProductId value, $Res Function(_ProductId) then) =
      __$ProductIdCopyWithImpl<$Res>;
  @override
  $Res call(
      {int id,
      String title,
      int price,
      String description,
      String createdAt,
      List<Pictures> pictures,
      Seller seller});

  @override
  $SellerCopyWith<$Res> get seller;
}

/// @nodoc
class __$ProductIdCopyWithImpl<$Res> extends _$ProductIdCopyWithImpl<$Res>
    implements _$ProductIdCopyWith<$Res> {
  __$ProductIdCopyWithImpl(_ProductId _value, $Res Function(_ProductId) _then)
      : super(_value, (v) => _then(v as _ProductId));

  @override
  _ProductId get _value => super._value as _ProductId;

  @override
  $Res call({
    Object? id = freezed,
    Object? title = freezed,
    Object? price = freezed,
    Object? description = freezed,
    Object? createdAt = freezed,
    Object? pictures = freezed,
    Object? seller = freezed,
  }) {
    return _then(_ProductId(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      createdAt: createdAt == freezed
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String,
      pictures: pictures == freezed
          ? _value.pictures
          : pictures // ignore: cast_nullable_to_non_nullable
              as List<Pictures>,
      seller: seller == freezed
          ? _value.seller
          : seller // ignore: cast_nullable_to_non_nullable
              as Seller,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ProductId extends _ProductId {
  const _$_ProductId(
      {required this.id,
      required this.title,
      required this.price,
      required this.description,
      required this.createdAt,
      required this.pictures,
      required this.seller})
      : super._();

  factory _$_ProductId.fromJson(Map<String, dynamic> json) =>
      _$$_ProductIdFromJson(json);

  @override
  final int id;
  @override
  final String title;
  @override
  final int price;
  @override
  final String description;
  @override
  final String createdAt;
  @override
  final List<Pictures> pictures;
  @override
  final Seller seller;

  @override
  String toString() {
    return 'ProductId(id: $id, title: $title, price: $price, description: $description, createdAt: $createdAt, pictures: $pictures, seller: $seller)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ProductId &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.price, price) &&
            const DeepCollectionEquality()
                .equals(other.description, description) &&
            const DeepCollectionEquality().equals(other.createdAt, createdAt) &&
            const DeepCollectionEquality().equals(other.pictures, pictures) &&
            const DeepCollectionEquality().equals(other.seller, seller));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(price),
      const DeepCollectionEquality().hash(description),
      const DeepCollectionEquality().hash(createdAt),
      const DeepCollectionEquality().hash(pictures),
      const DeepCollectionEquality().hash(seller));

  @JsonKey(ignore: true)
  @override
  _$ProductIdCopyWith<_ProductId> get copyWith =>
      __$ProductIdCopyWithImpl<_ProductId>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ProductIdToJson(this);
  }
}

abstract class _ProductId extends ProductId {
  const factory _ProductId(
      {required int id,
      required String title,
      required int price,
      required String description,
      required String createdAt,
      required List<Pictures> pictures,
      required Seller seller}) = _$_ProductId;
  const _ProductId._() : super._();

  factory _ProductId.fromJson(Map<String, dynamic> json) =
      _$_ProductId.fromJson;

  @override
  int get id;
  @override
  String get title;
  @override
  int get price;
  @override
  String get description;
  @override
  String get createdAt;
  @override
  List<Pictures> get pictures;
  @override
  Seller get seller;
  @override
  @JsonKey(ignore: true)
  _$ProductIdCopyWith<_ProductId> get copyWith =>
      throw _privateConstructorUsedError;
}
