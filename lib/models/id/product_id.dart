import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:marketplace_bsa/models/id/pictures.dart';
import 'package:marketplace_bsa/models/id/seller.dart';

part 'product_id.freezed.dart';
part 'product_id.g.dart';

@freezed
class ProductId with _$ProductId {
  const ProductId._();
  @JsonSerializable(explicitToJson: true)
  const factory ProductId({
    required int id,
    required String title,
    required int price,
    required String description,
    required String createdAt,
    required List<Pictures> pictures,
    required Seller seller,
  }) = _ProductId;

  factory ProductId.fromJson(Map<String, dynamic> json) =>
      _$ProductIdFromJson(json);
}
