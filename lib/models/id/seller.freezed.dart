// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'seller.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Seller _$SellerFromJson(Map<String, dynamic> json) {
  return _Seller.fromJson(json);
}

/// @nodoc
class _$SellerTearOff {
  const _$SellerTearOff();

  _Seller call(
      {required int? id,
      required String? title,
      required int? price,
      required String? description,
      required String? createdAt}) {
    return _Seller(
      id: id,
      title: title,
      price: price,
      description: description,
      createdAt: createdAt,
    );
  }

  Seller fromJson(Map<String, Object?> json) {
    return Seller.fromJson(json);
  }
}

/// @nodoc
const $Seller = _$SellerTearOff();

/// @nodoc
mixin _$Seller {
  int? get id => throw _privateConstructorUsedError;
  String? get title => throw _privateConstructorUsedError;
  int? get price => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  String? get createdAt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SellerCopyWith<Seller> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SellerCopyWith<$Res> {
  factory $SellerCopyWith(Seller value, $Res Function(Seller) then) =
      _$SellerCopyWithImpl<$Res>;
  $Res call(
      {int? id,
      String? title,
      int? price,
      String? description,
      String? createdAt});
}

/// @nodoc
class _$SellerCopyWithImpl<$Res> implements $SellerCopyWith<$Res> {
  _$SellerCopyWithImpl(this._value, this._then);

  final Seller _value;
  // ignore: unused_field
  final $Res Function(Seller) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? title = freezed,
    Object? price = freezed,
    Object? description = freezed,
    Object? createdAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: createdAt == freezed
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$SellerCopyWith<$Res> implements $SellerCopyWith<$Res> {
  factory _$SellerCopyWith(_Seller value, $Res Function(_Seller) then) =
      __$SellerCopyWithImpl<$Res>;
  @override
  $Res call(
      {int? id,
      String? title,
      int? price,
      String? description,
      String? createdAt});
}

/// @nodoc
class __$SellerCopyWithImpl<$Res> extends _$SellerCopyWithImpl<$Res>
    implements _$SellerCopyWith<$Res> {
  __$SellerCopyWithImpl(_Seller _value, $Res Function(_Seller) _then)
      : super(_value, (v) => _then(v as _Seller));

  @override
  _Seller get _value => super._value as _Seller;

  @override
  $Res call({
    Object? id = freezed,
    Object? title = freezed,
    Object? price = freezed,
    Object? description = freezed,
    Object? createdAt = freezed,
  }) {
    return _then(_Seller(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      createdAt: createdAt == freezed
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Seller extends _Seller {
  const _$_Seller(
      {required this.id,
      required this.title,
      required this.price,
      required this.description,
      required this.createdAt})
      : super._();

  factory _$_Seller.fromJson(Map<String, dynamic> json) =>
      _$$_SellerFromJson(json);

  @override
  final int? id;
  @override
  final String? title;
  @override
  final int? price;
  @override
  final String? description;
  @override
  final String? createdAt;

  @override
  String toString() {
    return 'Seller(id: $id, title: $title, price: $price, description: $description, createdAt: $createdAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Seller &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.price, price) &&
            const DeepCollectionEquality()
                .equals(other.description, description) &&
            const DeepCollectionEquality().equals(other.createdAt, createdAt));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(price),
      const DeepCollectionEquality().hash(description),
      const DeepCollectionEquality().hash(createdAt));

  @JsonKey(ignore: true)
  @override
  _$SellerCopyWith<_Seller> get copyWith =>
      __$SellerCopyWithImpl<_Seller>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SellerToJson(this);
  }
}

abstract class _Seller extends Seller {
  const factory _Seller(
      {required int? id,
      required String? title,
      required int? price,
      required String? description,
      required String? createdAt}) = _$_Seller;
  const _Seller._() : super._();

  factory _Seller.fromJson(Map<String, dynamic> json) = _$_Seller.fromJson;

  @override
  int? get id;
  @override
  String? get title;
  @override
  int? get price;
  @override
  String? get description;
  @override
  String? get createdAt;
  @override
  @JsonKey(ignore: true)
  _$SellerCopyWith<_Seller> get copyWith => throw _privateConstructorUsedError;
}
