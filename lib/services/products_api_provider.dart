import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:marketplace_bsa/models/product.dart';

class ProductsProvider {
  Future<List<Product>> loadProducts() async {
    final response = await http.get(
        Uri.parse('https://bsa-marketplace.azurewebsites.net/api/products'));
    if (response.statusCode == 200) {
      final List<dynamic> productJson = json.decode(response.body);

      return productJson
          .map((json) => Product.fromJson(Map<String, dynamic>.from(json)))
          .toList();
    } else {
      throw Exception('Error load');
    }
  }
}
