import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:marketplace_bsa/models/id/product_id.dart';

class ProductIdProvider {
  Future<ProductId> loadProduct(int index) async {
    final String url =
        'https://bsa-marketplace.azurewebsites.net/api/products/${index}';
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final dynamic productJson = json.decode(response.body);

      return ProductId.fromJson(Map<String, dynamic>.from(productJson));
    } else {
      throw Exception('Error load');
    }
  }
}

