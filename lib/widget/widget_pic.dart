import 'package:flutter/material.dart';

class WidgetPic extends StatelessWidget {
  final String? picture;
  const WidgetPic({Key? key, required this.picture}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return picture != null
        ? Image.network(
            picture!,
            width: 250,
            height: 250,
          )
        : Icon(
            Icons.image,
            size: 140,
            color: Colors.black12,
          );
  }
}
